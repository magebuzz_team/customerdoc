<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Model_Mysql4_Subcategory_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract{
    protected function _construct() {
        parent::_construct();
        $this->_init('customerdoc/subcategory');
    }

    public function toOptionArray($categoryId)
    {
        return $this->_toOptionArray($categoryId);
    }

    protected function _toOptionArray($categoryId,$valueField='category_id',$labelField='category_name',$additional=array())
    {
        $res = array();
        $additional['value'] = $valueField;
        $additional['label'] = $labelField;

        $this->addFieldToFilter('parent_category', $categoryId);
        foreach ($this as $item) {
            foreach ($additional as $code => $field) {
                $data[$code] = $item->getData($field);
            }
            $res[] = $data;
        }
        $defaultValue = array('value'=>'','label'=>'Please select a category');
        array_unshift($res, $defaultValue);

        return $res;
    }

    public function toOptionCategoriesArray(){

        return $this->_toOptionCategoriesArray();

    }

    protected function _toOptionCategoriesArray($valueField='category_id', $labelField='category_name', $additional=array()){

        $res = array();
        $additional['value'] = $valueField;
        $additional['label'] = $labelField;
        $categoriesData = $this->addFieldToFilter('level','0');
        foreach ($categoriesData as $item) {
            foreach ($additional as $code => $field) {
                $data[$code] = $item->getData($field);
            }
            $res[] = $data;
        }
        $defaultValue = array('value'=>'','label'=>'Please select a category');
        array_unshift($res, $defaultValue);

        return $res;

    }
}