<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Model_Mysql4_Documententity extends Mage_Core_Model_Mysql4_Abstract{

    protected function _construct() {
        $this->_init('customerdoc/documententity', 'document_entity_id');
    }
}