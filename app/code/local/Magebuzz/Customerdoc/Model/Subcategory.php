<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Model_Subcategory extends Mage_Core_Model_Abstract{
    protected function _construct() {
        parent::_construct();
        $this->_init('customerdoc/subcategory');
    }
}