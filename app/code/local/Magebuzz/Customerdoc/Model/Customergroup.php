<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Model_Customergroup extends Mage_Core_Model_Abstract {

  protected function _construct() {
    parent::_construct();
    $this->_init('customerdoc/customergroup');
  }

  public function saveDocumentCusGroup($newCgIds, $docId)
  {
    $oldDocumentCgModel = $this->getCollection()->addFieldToFilter('document_id',$docId);
    $oldCgIds = $oldDocumentCgModel->getColumnValues('customergroup_id');
    $insert = array_diff($newCgIds, $oldCgIds);
    $delete = array_diff($oldCgIds, $newCgIds);

    if(!$newCgIds){
      $deleteCg = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('document_id', $docId);
      if (count($deleteCg->getData()) > 0) {
        foreach ($deleteCg as $del) {
          $del->delete();
        }
      }
      return;
    }

    if ($delete) {
      $deleteCg = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', array('in' => $delete))
        ->addFieldToFilter('document_id', $docId);
      if (count($deleteCg->getData()) > 0) {
        foreach ($deleteCg as $del) {
          $del->delete();
        }
      }
    }
    if ($insert) {
      foreach ($insert as $groupId) {
        $cgSelect = Mage::getModel('customerdoc/customergroup');
        $cgSelect->setCustomergroupId($groupId);
        $cgSelect->setDocumentId($docId);
        $cgSelect->save();
        if(Mage::helper('customerdoc')->canSendEmail($docId)){
          $cgSelect->sendEmailToNewCustomer($groupId, $docId);
        }
      }
    }
  }

  public function sendEmailToNewCustomer($groupId, $docId)
  {
    $document = Mage::getModel('customerdoc/document')->load($docId);
    if($document->getDocumentLink()){
      $link = 'Download link: '.Mage::helper("adminhtml")->getUrl('customerdoc/adminhtml_document/download', array('id' => $docId));
    }else{
      $link = '';
    }
    $customers= Mage::getModel('customer/customer')
      ->getCollection()
      ->addAttributeToSelect('*')
      ->addFieldToFilter('group_id', $groupId);
    foreach ($customers as $customer) {
      $customerStoreId = $customer->getStoreId();
      $mailTemplate = Mage::getModel('core/email_template');
      /* @var $mailTemplate Mage_Core_Model_Email_Template */
      $mailTemplate->sendTransactional(
        Mage::getStoreConfig('customerdoc/document_options/email_template', $customerStoreId),
        Mage::getStoreConfig('contacts/email/sender_email_identity', $customerStoreId),
        $customer->getEmail(),
        null,
        array(
          'customername' => $customer->getFirstname(),
          'name' => $document->getName(),
          'description' => $document->getDescription(),
          'link' => $link
        )
      );
    }
  }
}