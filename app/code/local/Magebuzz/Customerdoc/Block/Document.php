<?php

/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */
class Magebuzz_Customerdoc_Block_Document extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('customerdoc/document.phtml');
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerId = $customer->getId();
        $groupId = $customer->getGroupId();
        $documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', $customerId)
            ->getColumnValues('document_id');

        $documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', $groupId)
            ->getColumnValues('document_id');
        $documentIds = array_merge($documentCustomerIds, $documentGroupIds);
        $documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('status', 1)
            ->addFieldToFilter('id', array('in' => $documentIds));
        if ($params = $this->getRequest()->getPost()) {
            Mage::getSingleton('core/session')->setFilterRequest($params);
            foreach ($params as $key => $param) {
                if (($key == 'description' || $key == 'name') && $param != '') {
                    $documents->addFieldToFilter($key, array('like' => '%' . $param . '%'));
                } elseif ($key == 'category' && $param !== '') {
                    if ($param == '-1') {
                        Mage::getSingleton('core/session')->setEmptyFilter(true);
                    } elseif ($param == '0') {
                        $modelCategoryDisable = Mage::getModel('customerdoc/subcategory')->getCollection()
                            ->addFieldToFilter('level', '0')
                            ->addFieldToFilter('status', '2')
                            ->getAllIds();
                        $documentIds = Mage::getModel('customerdoc/document')->getCollection()
                            ->addFieldToFilter('status', 1)
                            ->addFieldToFilter('category', array('in' => $modelCategoryDisable))->getAllIds();
                        $documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $documentIds));
                    } else {
                        $documents->addFieldToFilter('category', $param);
                    }
                } elseif ($key == 'subcategory' && $param !== '') {
                    if ($param == '-1') {
                        Mage::getSingleton('core/session')->setEmptyFilter(true);
                    } elseif ($param == '0') {
                        $modelSubcategoryDisable = Mage::getModel('customerdoc/subcategory')->getCollection()
                            ->addFieldToFilter('level', '0')
                            ->addFieldToFilter('status', '2')
                            ->getAllIds();
                        $documentIds = Mage::getModel('customerdoc/document')->getCollection()
                            ->addFieldToFilter('status', 1)
                            ->addFieldToFilter('subcategory', array('in' => $modelSubcategoryDisable))->getAllIds();
                        $documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $documentIds));
                    } else {
                        $documents->addFieldToFilter('subcategory', $param);
                    }

                    $documentIds = array_merge($documentCustomerIds, $documentGroupIds);
                }

            }

            $from = $params['from'];
            $timeFrom = date('Y/m/d H:i:s', strtotime($from));
            $to = $params['to'];
            $timeTo = date('Y/m/d H:i:s', strtotime($to));
            if (($from !== '') && ($to !== '')) {
                $documents->addFieldToFilter('created_time', array(
                    'from' => $timeFrom,
                    'to' => $timeTo
                ));
            }
            if (count($documents) < 1) {
                Mage::getSingleton('core/session')->setEmptyFilter(true);
            }
        }
        if ($data = $this->getRequest()->getParam('category')) {
            $documents->setOrder('category', $data);
        }

        $categoryIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $documentIds))
            ->getColumnValues('category');
        if (is_array($categoryIds)) {
            $categoryIds = array_unique($categoryIds);
            foreach ($categoryIds as $key => $catId) {
                if (Mage::helper('customerdoc')->isCategoryEnabled($catId) !== '1') {
                    $categoryIds[$key] = '0';
                }
            }
            $this->setCategoryIds(array_unique($categoryIds));
        }

        $subcategoryIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $documentIds))
            ->getColumnValues('subcategory');
        if (is_array($subcategoryIds)) {
            $subcategoryIds = array_unique($subcategoryIds);
            foreach ($subcategoryIds as $key => $subcatId) {
                if (Mage::helper('customerdoc')->isSubcategoryEnabled($subcatId) !== '1') {
                    $subcategoryIds[$key] = '0';
                }
            }
            $this->setSubcategoryIds(array_unique($subcategoryIds));
        }
        $this->setDocuments($documents);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerId = $customer->getId();
        $groupId = $customer->getGroupId();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'documents.pager')
            ->setCollection($this->getDocuments());
        $this->setChild('pager', $pager);
        $this->getDocuments()->load();
        $subcategoryIds = $this->getSubcategoryIds();
        if (count($subcategoryIds) > 0) {
            foreach ($subcategoryIds as $categoryId) {
                $catDocuments = Mage::helper('customerdoc')->loadDocCollectionByCatId($categoryId, $customerId, $groupId);
                $catPager = $this->getLayout()->createBlock('page/html_pager', 'documents.pager')
                    ->setCollection($catDocuments);
                $childName = 'pager' . $categoryId;
                $this->setChild($childName, $catPager);
                $catDocuments->load();
            }
        }

        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCategoryPager($catId)
    {
        $childName = 'pager' . $catId;
        return $this->getChildHtml($childName);
    }
}