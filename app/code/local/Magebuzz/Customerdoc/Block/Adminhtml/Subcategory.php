<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Block_Adminhtml_Subcategory extends Mage_Adminhtml_Block_Widget_Grid_Container{

    public function __construct() {
        $this->_controller = 'adminhtml_subcategory';
        $this->_blockGroup = 'customerdoc';
        $this->_headerText = Mage::helper('customerdoc')->__('Manage Categories');
        $this->_addButtonLabel = Mage::helper('customerdoc')->__('Add Category');
        parent::__construct();
    }
}