<?php

/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */
class Magebuzz_Customerdoc_Block_Adminhtml_Subcategory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('adminhtml_subcategory');
        $this->setDefaultSort('category_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(TRUE);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('customerdoc/subcategory')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('category_id', array(
            'header' => Mage::helper('customerdoc')->__('ID'),
            'index' => 'category_id',

        ));

        $this->addColumn('subcategory_name', array(
            'header' => Mage::helper('customerdoc')->__('Name'),
            'index' => 'category_name',
        ));

        $this->addColumn('level', array(
            'header' => Mage::helper('customerdoc')->__('Level'),
            'index' => 'level',
            'type' => 'options',
            'options' => array(
                0 => 'Parent Category',
                1 => 'Subcategory',
            ),
        ));

        $this->addColumn('parent_category', array(
            'header'   => Mage::helper('customerdoc')->__('Parent Category (search by ID)'),
            'index' => 'parent_category',
            'renderer' => 'customerdoc/adminhtml_subcategory_renderer_categoryname',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('customerdoc')->__('Status'),
            'align' => 'left',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('subcategory');

        $this->getMassactionBlock()->addItem('delete',
            array('label' => Mage::helper('adminhtml')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('adminhtml')->__('Are you sure?')));

        $statuses = Mage::getSingleton('customerdoc/status')->getOptionArray();

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('adminhtml')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => TRUE)),
            'additional' => array('visibility' => array('name' => 'status', 'type' => 'select', 'class' => 'required-entry', 'label' => Mage::helper('customerdoc')->__('Status'), 'values' => $statuses))));

        return $this;
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}