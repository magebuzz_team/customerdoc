<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Block_Adminhtml_Subcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('customerdoc_form', array('legend' => Mage::helper('customerdoc')->__('Category information')));

        if(Mage::registry('customerdoc_subcategory')) {
            $data = Mage::registry('customerdoc_subcategory')->getData();
        }

        $fieldset->addField('category_name', 'text', array(
            'label'     => Mage::helper('customerdoc')->__('Category Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'category_name',
        ));

        $event = $fieldset->addField('level', 'select', array(
            'name' => 'level',
            'label'     => Mage::helper('customerdoc')->__('Level'),
            'required'  => true,
            'onchange'  => 'hideShowCategoryOptions(this);',
            'values'    => array(
                0 => Mage::helper('customerdoc')->__('Parent Category'),
                1 => Mage::helper('customerdoc')->__('Subcategory'),
            )
        ));

        $event->setAfterElementHtml("<script type=\"text/javascript\">

function hideShowCategoryOptions()
{
    if ($('level').value == 1) {
        $('parent_category').up('tr').show();
        $('parent_category').addClassName('required-entry');
    } else {
        $('parent_category').up('tr').hide();
        $('parent_category').removeClassName('required-entry');
    }

    return true;
}
document.observe(\"dom:loaded\", hideShowCategoryOptions);
</script>");

        $docCategories = Mage::getModel('customerdoc/subcategory')->getCollection()->toOptionCategoriesArray();

        $fieldset->addField('parent_category', 'select', array(
            'name' => 'parent_category',
            'label' => Mage::helper('customerdoc')->__('Parent Category'),
            'values' => $docCategories
        ));



        $fieldset->addField('status', 'select', array(
                'label' => Mage::helper('customerdoc')->__('Status'),
                'name' => 'status',
                'values' => array(
                    array('value' => 1, 'label' => Mage::helper('customerdoc')->__('Enabled'),),
                    array('value' => 2, 'label' => Mage::helper('customerdoc')->__('Disabled'),)
                ,)
            ,)
        );

        $form->setValues($data);
        return parent::_prepareForm();

    }


}