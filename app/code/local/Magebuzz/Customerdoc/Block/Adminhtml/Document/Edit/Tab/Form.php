<?php

/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */
class Magebuzz_Customerdoc_Block_Adminhtml_Document_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('customerdoc_form', array('legend' => Mage::helper('customerdoc')->__('Document information')));

        if (Mage::registry('customerdoc_document')) {
            $data = Mage::registry('customerdoc_document')->getData();
            $document_link = Mage::registry('customerdoc_document')->getDocumentLink();
            $docId = Mage::registry('customerdoc_document')->getId();
            $docCusGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('document_id', $docId)->getColumnValues('customergroup_id');
            $data['customergroup_id'] = $docCusGroupIds;
            if ($data['thumbnail']) {
                $data['thumbnail'] = 'document/thumbnail/' . $data['thumbnail'];
            }
//      if(!$data['created_time']){
//        // var_dump(date("m-d-Y H:i:s", Mage::getModel('core/date')->timestamp(time())));die;
//        $data['created_time'] = date("m-d-Y H:i:s", Mage::getModel('core/date')->timestamp(time()));
//      }
        }

        $fieldset->addField('name', 'text', array(
            'label' => Mage::helper('customerdoc')->__('File Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('description', 'textarea', array(
            'label' => Mage::helper('customerdoc')->__('File Description'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'description',
        ));

        $fieldset->addType('file', Mage::getConfig()->getBlockClassName('customerdoc/adminhtml_document_helper_file'));

        if ($document_link) {
            $fieldset->addField('document_link', 'file', array(
                'label' => Mage::helper('customerdoc')->__('File Upload'),
                'required' => false,
                'name' => 'document_link',
                'value' => 'document_link'
            ));
        } else {
            $fieldset->addField('document_link', 'file', array(
                'label' => Mage::helper('customerdoc')->__('File Upload'),
                'required' => true,
                'name' => 'document_link',
                'value' => 'document_link'
            ));
        }


        $fieldset->addField('thumbnail', 'image',
            array(
                'name' => 'thumbnail',
                'label' => Mage::helper('customerdoc')->__('Thumbnail'),
                'value' => $data['thumbnail'],
            ));

        $fieldset->addField('status', 'select', array(
                'label' => Mage::helper('customerdoc')->__('Status'),
                'name' => 'status',
                'values' => array(
                    array('value' => 1, 'label' => Mage::helper('customerdoc')->__('Enabled'),),
                    array('value' => 2, 'label' => Mage::helper('customerdoc')->__('Disabled'),)
                ,)
            ,)
        );

        $fieldset->addField('send_email', 'select', array(
                'label' => Mage::helper('customerdoc')->__('Send Email'),
                'name' => 'send_email',
                'values' => array(
                    array('value' => 2, 'label' => Mage::helper('customerdoc')->__('No'),),
                    array('value' => 1, 'label' => Mage::helper('customerdoc')->__('Yes'),)
                ,)
            ,)
        );

        $docCategories = Mage::getModel('customerdoc/subcategory')->getCollection()->toOptionCategoriesArray();
        $event = $fieldset->addField('category', 'select', array(
            'name' => 'category',
            'label' => Mage::helper('customerdoc')->__('Category'),
            'required' => true,
            'values' => $docCategories,
            'onchange'  => 'checkSelectedItem(this)',
        ));

        $event->setAfterElementHtml("<script type=\"text/javascript\">
    function checkSelectedItem(selectElement){
        var reloadurl = '" . $this->getUrl('customerdoc/adminhtml_document/selectedcategory') . "category/' + selectElement.value;
        new Ajax.Request(reloadurl, {
            method: 'get',
            onLoading: function (transport) {
                $('subcategory').update('Searching...');
            },
            onComplete: function(transport) {
                    $('subcategory').update(transport.responseText);
            }
        });
    }
</script>");

        $docSubcategories = Mage::getModel('customerdoc/subcategory')->getCollection()->toOptionArray($data['category']);

        $fieldset->addField('subcategory', 'select', array(
            'name' => 'subcategory',
            'label' => Mage::helper('customerdoc')->__('Subcategory'),

            'values' => $docSubcategories
        ));

        $customerGroupArr = Mage::getModel('customer/group')->getCollection()->toOptionArray();
        unset($customerGroupArr[0]);

        $fieldset->addField('customergroup_id', 'multiselect', array(
            'name' => 'customergroup_id',
            'label' => 'Group',
            'required' => true,
            'values' => $customerGroupArr
        ));

        $form->setValues($data);
        return parent::_prepareForm();
    }
}