<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Block_Adminhtml_Document_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

  public function __construct() {
    parent::__construct();
    $this->setId('document_tabs');
    $this->setDestElementId('edit_form');
    $this->setTitle(Mage::helper('customerdoc')->__('Document Information'));
  }

  protected function _beforeToHtml() {
    $this->addTab('form_section', array(
      'label' => Mage::helper('customerdoc')->__('Document Information'),
      'title' => Mage::helper('customerdoc')->__('Document Information'),
      'content' => $this->getLayout()->createBlock('customerdoc/adminhtml_document_edit_tab_form')->toHtml(),
    ));

      $this->addTab('document_customer', array(
          'label' => Mage::helper('customerdoc')->__('Customer Document'),
          'title' => Mage::helper('customerdoc')->__('Customer Document'),
//          'content' => $this->getLayout()->createBlock('customerdoc/adminhtml_document_edit_tab_customer')->toHtml(),
          'url' => $this->getUrl('customerdoc/adminhtml_document/dcustomer', array('_current' => true)),
          'class' => 'ajax',
      ));

    return parent::_beforeToHtml();
  }
}