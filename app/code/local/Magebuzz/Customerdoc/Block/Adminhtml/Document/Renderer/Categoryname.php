<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Block_Adminhtml_Document_Renderer_Categoryname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $categoryNameId = $row->getCategory();

        $categoryModel = Mage::getModel('customerdoc/subcategory')->getCollection()
            ->addFieldToFilter('category_id', $categoryNameId);
        $categoryName = '';
        if (count($categoryModel)) {
            foreach ($categoryModel as $_categoryModel) {
                $categoryId = $_categoryModel->getCategoryId();
                $categoryName .= Mage::getModel('customerdoc/subcategory')->load($categoryId)->getCategoryName();
            }
        }

        return $categoryName;
    }
}