<?php

/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */
class Magebuzz_Customerdoc_Block_Adminhtml_Document_Edit_Tab_Customer extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('list_document_customer');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(TRUE);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('customer/customer_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'selected_customer') {
            $customer = $this->_getSelectedCustomer();
            if (empty($customer)) {
                $customer = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $customer));
            } else {
                if ($customer) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $customer));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('selected_customer', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'field_name' => 'selected_customer[]',
            'values' => $this->_getSelectedCustomer(),
            'align' => 'center',
            'index' => 'entity_id'
        ));

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('customer')->__('ID'),
            'width' => '20',
            'index' => 'entity_id',
            'type' => 'number',
        ));

        $this->addColumn('cus_email', array(
            'header' => Mage::helper('customer')->__('Email'),
            'width' => '150',
            'index' => 'email'
        ));

        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt' => 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('cus_group', array(
            'header' => Mage::helper('customer')->__('Group'),
            'width' => '100',
            'index' => 'group_id',
            'type' => 'options',
            'options' => $groups,
        ));

        return parent::_prepareColumns();
    }


    protected function _getSelectedCustomer()
    {
        $customerIds = $this->getSelectedCustomer();
        return $customerIds;
    }


    public function getSelectedCustomer()
    {
        $docId = $this->getRequest()->getParam('id');
        $documentCustomers = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('document_id', $docId);
        $customerIds = $documentCustomers->getColumnValues('customer_id');
        return $customerIds;
    }
}