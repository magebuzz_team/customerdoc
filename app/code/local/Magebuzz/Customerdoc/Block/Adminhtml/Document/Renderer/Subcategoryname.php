<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

class Magebuzz_Customerdoc_Block_Adminhtml_Document_Renderer_Subcategoryname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $subcategoryNameId = $row->getSubcategory();


        $subcategoryModel = Mage::getModel('customerdoc/subcategory')->getCollection()
            ->addFieldToFilter('category_id', $subcategoryNameId);
        $subcategoryName = '';
        if (count($subcategoryModel)) {
            foreach ($subcategoryModel as $_subcategoryModel) {
                $subcategoryId = $_subcategoryModel->getCategoryId();
                $subcategoryName .= Mage::getModel('customerdoc/subcategory')->load($subcategoryId)->getCategoryName();
            }
        }

        return $subcategoryName;
    }
}