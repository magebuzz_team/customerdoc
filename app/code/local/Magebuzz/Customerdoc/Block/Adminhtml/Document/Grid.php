<?php

	/**
	 * @copyright Copyright (c) 2015 www.magebuzz.com
	 */
	class Magebuzz_Customerdoc_Block_Adminhtml_Document_Grid extends Mage_Adminhtml_Block_Widget_Grid
	{
		public function __construct()
		{
			parent::__construct();
			$this->setId('adminhtml_document');
			$this->setDefaultSort('id');
			$this->setDefaultDir('ASC');
			$this->setSaveParametersInSession(TRUE);
		}

		protected function _prepareCollection()
		{
			$collection = Mage::getModel('customerdoc/document')->getCollection();
			$this->setCollection($collection);
			return parent::_prepareCollection();
		}

		protected function _prepareColumns()
		{
			$this->addColumn('id', array(
			 'header' => Mage::helper('customerdoc')->__('ID'),
			 'index' => 'id',
			));

			$this->addColumn('name', array(
			 'header' => Mage::helper('customerdoc')->__('Name'),
			 'index' => 'name',
			));

			$this->addColumn('description', array(
			 'header' => Mage::helper('customerdoc')->__('Description'),
			 'index' => 'description',
			));

			$this->addColumn('category', array(
				'header'   => Mage::helper('customerdoc')->__('Category Name (search by ID)'),
				'index' => 'category',
				'renderer' => 'customerdoc/adminhtml_document_renderer_categoryname',

			));

			$this->addColumn('subcategory', array(
				'header'   => Mage::helper('customerdoc')->__('Subcategory Name (search by ID)'),
				'index' => 'subcategory',
				'renderer' => 'customerdoc/adminhtml_document_renderer_subcategoryname',
			));

			$this->addColumn('status', array(
			 'header' => Mage::helper('customerdoc')->__('Status'),
			 'align' => 'left',
			 'index' => 'status',
			 'type' => 'options',
			 'options' => array(
			  1 => 'Enabled',
			  2 => 'Disabled',
			 ),
			));

			$link = Mage::helper('adminhtml')->getUrl('customerdoc/adminhtml_document/edit/') . 'id/$id';
			$this->addColumn('action_edit', array(
			 'header' => $this->helper('customerdoc')->__('Action'),
			 'width' => 15,
			 'sortable' => false,
			 'filter' => false,
			 'type' => 'action',
			 'actions' => array(
			  array(
			   'url' => $link,
			   'caption' => $this->helper('customerdoc')->__('Edit'),
			  ),
			 )
			));

			return parent::_prepareColumns();
		}

		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('document');

			$this->getMassactionBlock()->addItem('delete',
			 array('label' => Mage::helper('adminhtml')->__('Delete'),
			  'url' => $this->getUrl('*/*/massDelete'),
			  'confirm' => Mage::helper('adminhtml')->__('Are you sure?')));

			$statuses = Mage::getSingleton('customerdoc/status')->getOptionArray();

			array_unshift($statuses, array('label' => '', 'value' => ''));
			$this->getMassactionBlock()->addItem('status', array(
			 'label' => Mage::helper('adminhtml')->__('Change status'),
			 'url' => $this->getUrl('*/*/massStatus', array('_current' => TRUE)),
			 'additional' => array('visibility' => array('name' => 'status', 'type' => 'select', 'class' => 'required-entry', 'label' => Mage::helper('customerdoc')->__('Status'), 'values' => $statuses))));

			return $this;
		}

		public function getRowUrl($row)
		{
			return $this->getUrl('*/*/edit', array('id' => $row->getId()));
		}

	}