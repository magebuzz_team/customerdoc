<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

$installer = $this;
$installer->startSetup();

$installer->run("
  CREATE TABLE {$this->getTable('document_subcategory')} (
  `category_id` int(10) unsigned NOT NULL auto_increment,
  `category_name` text NOT NULL default '',
  `parent_category` smallint(6) NOT NULL default '0',
  `level` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL default '1',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");