<?php
/**
 * @copyright Copyright (c) 2015 www.magebuzz.com
 */

$installer = $this;
$installer->startSetup();

$installer->run("

  ALTER TABLE `{$this->getTable('document')}` ADD COLUMN `created_time` TIMESTAMP NULL;
");
$installer->endSetup();