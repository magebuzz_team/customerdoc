<?php

/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */
class Magebuzz_Customerdoc_Adminhtml_SubcategoryController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {

        $session = Mage::getSingleton('admin/session');
        return $session->isAllowed('customer/document/subcategory');
    }

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('customerdoc/items')->_addBreadcrumb(Mage::helper('adminhtml')->__('Document Subcategory'), Mage::helper('adminhtml')->__('Document Subcategory'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('customerdoc/subcategory');

        if ($id) {
            $model->load($id);
            $data = Mage::getSingleton('adminhtml/session')->getFormData(TRUE);
            if (!empty($data)) {
                $model->setData($data);
            }
        }
        Mage::register('customerdoc_subcategory', $model);
        $this->loadLayout();
        $this->_setActiveMenu('customerdoc/items');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(TRUE);

        $this->_addContent($this->getLayout()->createBlock('customerdoc/adminhtml_subcategory_edit'))->_addLeft($this->getLayout()->createBlock('customerdoc/adminhtml_subcategory_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_redirect('*/*/edit');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
                $model = Mage::getModel('customerdoc/subcategory');
                if ($id = $this->getRequest()->getParam('id')) {
                    $model->load($id);
                    $model->setData($data)->setId($this->getRequest()->getParam('id'));
                } else {
                    $model->setData($data);
                }

                try {
                    $model->save();
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully saved'));
                    Mage::getSingleton('adminhtml/session')->setFormData(FALSE);

                    if ($this->getRequest()->getParam('back')) {
                        $this->_redirect('*/*/edit', array('id' => $model->getId()));
                        return;
                    }

                    $this->_redirect('*/*/');
                    return;
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    Mage::getSingleton('adminhtml/session')->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }

        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('customerdoc')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('customerdoc/subcategory');

                $model->setId($this->getRequest()->getParam('id'))->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $subcategory = $this->getRequest()->getParam('subcategory');
        if (!is_array($subcategory)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($subcategory as $province) {
                    $provinceModel = Mage::getModel('customerdoc/subcategory')->load($province);
                    $provinceModel->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($subcategory)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $subcategoryIds = $this->getRequest()->getParam('subcategory');
        if (!is_array($subcategoryIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($subcategoryIds as $subcategoryId) {
                    Mage::getSingleton('customerdoc/subcategory')->load($subcategoryId)->setStatus($this->getRequest()->getParam('status'))->setIsMassupdate(TRUE)->save();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were successfully updated', count($subcategoryIds)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
}