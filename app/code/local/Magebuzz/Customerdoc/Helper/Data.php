<?php

	/**
	 * @copyright Copyright (c) 2015 www.magebuzz.com
	 */
	class Magebuzz_Customerdoc_Helper_Data extends Mage_Core_Helper_Abstract
	{
		public function renameFile($fileName)
		{
			$string = str_replace("  ", " ", $fileName);
			$newFileName = str_replace(" ", "-", $string);
			$newFileName = strtolower($newFileName);
			return $newFileName;
		}
		public function loadCategoryNameById($id)
		{
			$status = Mage::getModel('customerdoc/subcategory')->load($id)->getStatus();
			if ($status !== '1') {
				return '';
			}
			return Mage::getModel('customerdoc/subcategory')->load($id)->getCategoryName();
		}

		public function loadSubcategoryNameById($id)
		{
			$status = Mage::getModel('customerdoc/subcategory')->load($id)->getStatus();
			if ($status !== '1') {
				return '';
			}
			return Mage::getModel('customerdoc/subcategory')->load($id)->getCategoryName();
		}

		public function loadDocCollectionByCatId($catId, $customerId, $groupId)
		{
			$documentIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('status', 1)
			 ->addFieldToFilter('subcategory', $catId)->getAllIds();
			$documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', $groupId)->getColumnValues('document_id');
			$documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', $customerId)->getColumnValues('document_id');
			$result = array_unique(array_merge($documentCustomerIds, $documentGroupIds));
			$finalDocIds = array_intersect($result, $documentIds);
			$documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $finalDocIds));
			return $documents;
		}

		public function loadDocCollectionByCategoryId($catId, $customerId, $groupId)
		{
			$documentIds = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('status', 1)
				->addFieldToFilter('category', $catId)
				->addFieldToFilter('subcategory', '')
				->getAllIds();

			$documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', $groupId)->getColumnValues('document_id');
			$documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', $customerId)->getColumnValues('document_id');
			$result = array_unique(array_merge($documentCustomerIds, $documentGroupIds));
			$finalDocIds = array_intersect($result, $documentIds);
			$documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $finalDocIds));
			return $documents;
		}

		public function loadDocCollectionByCategoryIdZero($catId, $customerId, $groupId)
		{
			$modelCategoryDisable = Mage::getModel('customerdoc/subcategory')->getCollection()
				->addFieldToFilter('status', '2')
				->getAllIds();
			$documentIds = Mage::getModel('customerdoc/document')->getCollection()
				->addFieldToFilter('status', 1)
				->addFieldToFilter('category', array('in' => $modelCategoryDisable))
				->addFieldToFilter('subcategory', '')
				->getAllIds();
			$documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', $groupId)->getColumnValues('document_id');
			$documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', $customerId)->getColumnValues('document_id');
			$result = array_unique(array_merge($documentCustomerIds, $documentGroupIds));
			$finalDocIds = array_intersect($result, $documentIds);
			$documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $finalDocIds));
			return $documents;
		}

		public function loadDocCollectionByCatIdZero($catId, $customerId, $groupId)
		{
			$modelCategoryDisable = Mage::getModel('customerdoc/subcategory')->getCollection()
			 ->addFieldToFilter('status', '2')
			 ->getAllIds();
			$documentIds = Mage::getModel('customerdoc/document')->getCollection()
			 ->addFieldToFilter('status', 1)
			 ->addFieldToFilter('subcategory', array('in' => $modelCategoryDisable))->getAllIds();
			$documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()->addFieldToFilter('customergroup_id', $groupId)->getColumnValues('document_id');
			$documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()->addFieldToFilter('customer_id', $customerId)->getColumnValues('document_id');
			$result = array_unique(array_merge($documentCustomerIds, $documentGroupIds));
			$finalDocIds = array_intersect($result, $documentIds);
			$documents = Mage::getModel('customerdoc/document')->getCollection()->addFieldToFilter('id', array('in' => $finalDocIds));
			return $documents;
		}

		public function loadSubcategoryByCatId($catId){

			$modelSubcategory = Mage::getModel('customerdoc/subcategory')->getCollection()
				->addFieldToFilter('status',1)
				->addFieldToFilter('parent_category', $catId);

			return $modelSubcategory;
		}

		public function canSendEmail($docId)
		{
			$sendEmail = Mage::getModel('customerdoc/document')->load($docId)->getSendEmail();
			if ($sendEmail == 1) {
				return true;
			} else {
				return false;
			}
		}

		public function isCategoryEnabled($catId)
		{
			return Mage::getModel('customerdoc/subcategory')
				->load($catId)->getStatus();

		}

		public function isSubcategoryEnabled($subcatId)
		{
			return Mage::getModel('customerdoc/subcategory')
				->load($subcatId)->getStatus();

		}

		public function canAccessDocument($documentId)
		{
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			$customerId = $customer->getId();
			$groupId = $customer->getGroupId();
			$documentCustomerIds = Mage::getModel('customerdoc/customer')->getCollection()
			 ->addFieldToFilter('customer_id', $customerId)
			 ->getColumnValues('document_id');

			$documentGroupIds = Mage::getModel('customerdoc/customergroup')->getCollection()
			 ->addFieldToFilter('customergroup_id', $groupId)
			 ->getColumnValues('document_id');
			$documentIds = array_merge($documentCustomerIds, $documentGroupIds);
			if (in_array($documentId, $documentIds)) {
				return true;
			}
			return false;
		}

	   public function getIsoDate($str)
	   {
		  return Mage::app()->getLocale()->date($str)->getIso();
	   }
	}
